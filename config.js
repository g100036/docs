module.exports = {
  title: '一起学前端',
  description: '记录自己日常的学习知识',
  themeConfig: {
    nav: [
      { text: '首页', link: '/' },
      { text: '区块链', link: '/blockChain/solidity/' },
      { text: 'Vue', link: '/vue/Vue/' },
      { text: '设计模式', link: '/designMode/designMode/' }
    ],
    sidebar: {
      '/blockChain/': [
        {
          title: '区块链',
          collapsable: false,
          sidebarDepth: 3,
          children: ['solidity.md', 'web3.md', 'metaMask.md']
        }
      ],
      '/vue/': [
        {
          title: 'Vue',
          collapsable: false,
          sidebarDepth: 3,
          children: ['Vue.md', 'Vue-router.md', 'Vuex.md', 'Axios.md']
        }
      ],
      '/designMode/': [
        {
          title: '设计模式',
          collapsable: false,
          sidebarDepth: 5,
          children: ['designMode.md']
        }
      ]
    }
  }
}
